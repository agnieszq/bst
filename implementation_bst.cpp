#include "BST.hpp"
#include <iostream>
#include <string>

template<typename K, typename V>
  std::ostream& operator<<(std::ostream& out, BST<K,V>& bst) {
	  std::vector<K> thekeys = bst.keys();
	  for (K key : thekeys) {
		  out << key << " " << bst.get(bst.root, key) << std::endl;
	  }
	return out;
  }


int main() {
	BST<int, std::string> tree;
	tree[0] = "A";
	tree[1] = "G";
	tree[2] = "N";
	tree[3] = "I";
	std::cout << tree;

	//check pen and paper exercises
	int keys[] = {610, 13, 2, 55, 21, 89, 144377, 3, 1597, 4181, 8, 987};
	BST<int, std::string> keys_tree;
	for (int i : keys) {
		keys_tree[i];
	}
	std::cout << keys_tree;

}
